from django.db import models

# Create your models here.

class GlobalVar(models.Model):
    nama = models.CharField(max_length=50, unique=True)
    isi = models.CharField(max_length=200)