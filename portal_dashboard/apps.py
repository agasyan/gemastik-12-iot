from django.apps import AppConfig


class PortalDashboardConfig(AppConfig):
    name = 'portal_dashboard'
