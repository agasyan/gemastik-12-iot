from django.urls import path
from .views import index, get_data, change
#url for app

urlpatterns = [
    path('', index, name='index'),
    path('get-data/', get_data, name='get_data'),
    path('change/<str:mode_input>/<int:detik>', change, name='change'),
]