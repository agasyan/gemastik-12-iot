from django.shortcuts import render
from datetime import datetime, timedelta
from django.http import JsonResponse, HttpResponse
from .models import GlobalVar


def index(request):
    check_isi_var()
    return render(request, 'index.html')

def change(request, mode_input, detik):
    check_isi_var()
    if mode_input != "safe" and mode_input != "stop" and mode_input != "ada":
        return HttpResponse("Invalid Mode")
    else:
        now = datetime.now()
        now_plus = now + timedelta(seconds = detik)
        next_state = str(now_plus)  
        # Update DB
        GlobalVar.objects.filter(nama="next_state").update(isi=next_state)
        GlobalVar.objects.filter(nama="mode").update(isi=mode_input)

        # Print Out
        out =  "Success change mode to " + mode_input + "next state: " + next_state
        return HttpResponse(out)

def get_data(request):
    check_isi_var()
    now = datetime.now()
    next_state = GlobalVar.objects.get(nama="next_state").isi
    mode = GlobalVar.objects.get(nama="mode").isi
    
    next_datetime = datetime.strptime(next_state, '%Y-%m-%d %H:%M:%S.%f')
    difference = (next_datetime - now).total_seconds()
    if difference <= 0 and mode == "stop":
        mode = "safe"
        GlobalVar.objects.filter(nama="mode").update(isi=mode)

    return JsonResponse({'sisa': int(difference), 'mode': mode})

def check_isi_var():
    now = datetime.now()
    # Init if none mode
    try: 
        mode = GlobalVar.objects.get(nama="mode")
    except:
        var1 = GlobalVar(nama="mode", isi="safe")
        var1.save()
    
    # mode = GlobalVar.objects.get(nama="mode") --> cara ambil
    # Init if none next_state
    try: 
        next_state = GlobalVar.objects.get(nama="next_state")
    except:
        var2 = GlobalVar(nama = "next_state", isi=str(now))
        var2.save()