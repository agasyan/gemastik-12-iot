// Waktu

window.setTimeout("waktu()", 1000);
window.setTimeout("state()", 1000);

function waktu() {
    let tanggallengkap = new String();
    let namahari = ("Minggu Senin Selasa Rabu Kamis Jumat Sabtu");
    namahari = namahari.split(" ");
    let namabulan = ("Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember");
    namabulan = namabulan.split(" ");
    let tgl = new Date();
    let hari = tgl.getDay();
    let tanggal = tgl.getDate();
    let bulan = tgl.getMonth();
    let tahun = tgl.getFullYear();
    tanggallengkap = namahari[hari] + ", " + tanggal + " " + namabulan[bulan] + " " + tahun;
    var h = tgl.getHours();
    var m = tgl.getMinutes();
    var s = tgl.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    setTimeout("waktu()", 500);
    document.getElementById("tanggaljam").innerHTML = tanggallengkap + "<br>" + h + ":" + m + ":" + s;
}

function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function state() {
    let url = 'get-data/';

    fetch(url)
    .then(res => res.json())
    .then((out) => {
    mode = out.mode;
    sisa = out.sisa;
    if(mode == "safe"){
        document.getElementById("pesan").innerHTML = "Berhati-hatilah saat menyebrang rel kereta, jangan lupa lihat kiri dan kanan !";
        document.getElementById("countdown").innerHTML = "<p class= \"text-success\">Palang aman untuk dilewati, kereta masih jauh</p>";
        document.getElementById("detik").innerHTML = "";
    } else if(mode == "stop"){
        document.getElementById("pesan").innerHTML = "Stop! Jangan Menerobos ada kereta mau lewat";
        document.getElementById("countdown").innerHTML = "<p class=\"text-danger\">Palang tertutup dan akan terbuka dalam </p>"
        document.getElementById("detik").innerHTML = sisa + " detik";
    } else if(mode == "ada"){
        document.getElementById("pesan").innerHTML = "STOP!!!!!!  KERETA SUDAH SANGAT DEKAT";
        document.getElementById("countdown").innerHTML = "<p class=\"text-danger\">KERETA AKAN LEWAT DALAM WAKTU DEKAT </p>"
        document.getElementById("detik").innerHTML = "";
    }
    })
    .catch(err => { throw err });
    setTimeout("state()", 500);
}